import sys
import vnclic.vnclic

def main(args=None):
    """The main routine."""
    if args is None:
        args = sys.argv[1:]
    
    vnclic.vnclic.VnClic(args)

if __name__ == "__main__":
    main()
