import os
from setuptools import setup

setup(
    name = "vnclic",
    version = "0.1.1",
    author = "Ludovic Kiefer",
    author_email = "dev@lkiefer.org",
    description = "Screen sharing on LAN",
    license = "GNU GPL v2",
    url = "https://framagit.org/lkiefer/vnclic",
    packages=['vnclic'],
    entry_points = {
        'gui_scripts' : ['vnclic = vnclic.__main__:main',]
    },
    data_files = [
        ('/usr/share/applications/', ['vnclic.desktop']),
        ('/etc/', ['vnclic/vnclic.conf']),
    ],
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)",
    ]
)

# ~ packages=['myapp', 'myapp.models', 'myapp.classes', 'myapp.functions'],
# ~ package_data={'': ['myapp.conf']},
# ~ data_files=[('/usr/local/bin/', ['myapp/myapp_run.py']),
            # ~ ('/usr/local/etc/myapp', ['myapp/myapp.conf'])],
# ~ install_requires=['python3-tk', 'x11vnc', 'ssvnc']
