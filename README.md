## vnclic

vnclic is a simple desktop sharing software designed for education and computer club. **Do not use it on untrusted network:** users are not authentified and all data are send unencrypted.

![Screenshot: list of user sharing their desktop](screenshot/list.png "Interface when not sharing")
![Screenshot: sharing your desktop](screenshot/share.png "Interface when sharing")

Like italc or Epoptes, it let you share and control computers into a computer lab.
But unlike these softwares, vnclic is designed to be installed on personal computer:

- you do not need to configure a server
- each user will have the same rights
- no-one could access your computer without your permission

This software may only works on a IPv4 network and with X11 windowing system (not tested with Wayland).

## Future improvements

- Add english translation
- Add a chat system
- Add more options
- Remove inactive users form the list
- Make a deb package
- Make it compatible with Wayland

## Installation

- Clone the repository
- Make sure you have x11vnc, ssvnc, python3 and python3-tk, installed
- Run vnclic.py

## How it works

This software relies on the VNC protocol. It will ease the configuration of the VNC client (ssvnc) and the server (x11vnc).
When Alice clic on "Share my screen" the VNC server is started and a broadcast message is send to advertise other users.
When this message is received by another computer running vnclic, this computer may automatically show Alice's screen.
