#!/usr/bin/env python3

    #  Copyright Ludovic Kiefer 2020
    
    #  This file is part of vnclic.
      
    #  vnclic is free software: you can redistribute it and/or modify
    #  it under the terms of the GNU General Public License as published by
    #  the Free Software Foundation, either version 2 of the License, or
    #  (at your option) any later version.
      
    #  vnclic is distributed in the hope that it will be useful,
    #  but WITHOUT ANY WARRANTY; without even the implied warranty of
    #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #  GNU General Public License for more details.
      
    #  You should have received a copy of the GNU General Public License
    #  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

from os.path import expanduser
from os.path import isfile
import configparser
import sys

class Preference(object):
    "Manage User Preferences"
    
    def __init__(self, name):
        "find configfile depending of the OS"
        # software directory
        self.pathname=sys.path[0]
        
        try:
            from win32com.shell import shellcon, shell
            homedir = shell.SHGetFolderPath(0, shellcon.CSIDL_APPDATA, 0, 0)
        except:
            homedir = expanduser("~")
        
        # User preferences (hidden file starting with a dot)
        self.user_preference = homedir + "/." + name + ".conf"
        
        # Default preferences
        self.default_preference = "/etc/" + name + ".conf" #Works only on Linux
        if not isfile(self.default_preference):
            self.default_preference = self.pathname + "/" + name + ".conf"

    def save(self, config):
        try:
            with open(self.user_preference, 'w') as f:
                config.write(f)
                print("Save preferences into " + self.user_preference)
        except:
            print("Error writing preferences into " + self.user_preference)

    def load(self):
        config = configparser.ConfigParser()
        try:
            data = config.read(self.user_preference)
            # On vérifie que le fichier a été chargé
            if len(data) != 1:
                raise
            print("Loading preferences from "+ self.user_preference)
            return config
        except:
            return self.default()

    def default(self):
        config = configparser.ConfigParser()
        try:
            data = config.read(self.default_preference)
            # On vérifie que le fichier a été chargé
            if len(data) != 1:
                raise
            print("Loading default preferences")
            return config
        except:
            print("Unable to load default preferences")
            exit(0)
            return False

if __name__ == '__main__' :

    p = Preference('vnclic')
    p.load()
    print(p['Client']['Size'])


###p.save()
###config=p.load()
##config=p.default()
##p.save(config)
##print(config['Picture']['size_x'])
