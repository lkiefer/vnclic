#!/usr/bin/env python3

    #  Copyright Ludovic Kiefer 2020
    
    #  This file is part of vnclic.
      
    #  vnclic is free software: you can redistribute it and/or modify
    #  it under the terms of the GNU General Public License as published by
    #  the Free Software Foundation, either version 2 of the License, or
    #  (at your option) any later version.
      
    #  vnclic is distributed in the hope that it will be useful,
    #  but WITHOUT ANY WARRANTY; without even the implied warranty of
    #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #  GNU General Public License for more details.
      
    #  You should have received a copy of the GNU General Public License
    #  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
    
#
# Nécessite les logiciels X11VNC, ssvnc, et le paquet python3-tk
#
from tkinter import *
from tkinter.ttk import *
import tkinter as tk
import configparser
import sys, threading, time
import subprocess
try:
  from vnclic.network import *
  from vnclic.preference import *
except:
  from network import *
  from preference import *

class VnClic(object):
    """Share your screen on LAN"""
    
    def __init__(self, args=None):
        self.w = Tk()
        self.w.title('vnclic')

        # Folder containing the program files
        self.pathname=sys.path[0]
        # an object to manage preferences (load, save...)
        self.config = Preference('vnclic')
        # an configParser object containing user preferences
        self.preference = self.config.load()
        if self.preference == False:
            print("Please check your config file 'vnclic.conf'. To reset to default values,\
             just delete the .vnclic.conf file in your home folder.")
            Message(self.w,
            text = "Please check your config file 'vnclic.conf'. To reset to default values,\
             just delete the .vnclic.conf file in your home folder.",
            width=300, relief=FLAT)
            sys.exit(0)

        self.user_list = {}

        self.connect()
        self.started=False

        self.w.mainloop()

    def mainWindow(self):
        # Main frame
        sidebar = Frame(self.w)
        sidebar.pack(side =LEFT, padx =4, anchor=NW)

        topbar = Frame(sidebar)
        topbar.pack(side =TOP, padx =4)
        
        Button(topbar,  text="Quitter", command= self.quit).pack(side =RIGHT, padx =4)

        # Start or stop screen sharing
        f_toggle = Frame(topbar)
        f_toggle.pack(side =RIGHT, padx =4, pady =4)
        self.btn_start = tk.Button(f_toggle,  text="Partager mon écran", command= self.start)
        self.btn_start.pack(side =TOP)
        self.btn_stop = tk.Button(f_toggle,  text="Stopper le partage", command= self.stop, bg="#FF6666", fg="#000000")
        self.btn_stop.pack(side =TOP)
        self.btn_stop.pack_forget()

        optionbar = Frame(sidebar, relief= GROOVE, borderwidth=3)
        optionbar.pack(side =TOP, padx =4, anchor=NW, fill=X)
        Label(optionbar, text = "Options:").pack(side =TOP, padx =4, pady =4)

        self.name = StringVar()
        self.name.set(self.preference['Server'].get('name'))
        if self.name.get() == "":
            self.name.set(gethostname())
        f_name = Frame(optionbar)
        f_name.pack(side =TOP)
        Label(f_name, text = "Nom:").pack(side =LEFT, padx =4, pady =4)
        Entry(f_name, textvariable=self.name).pack(side =LEFT, padx =4)

        self.autoconnect = BooleanVar()
        self.autoconnect.set(self.preference['Client'].getboolean('autoconnect'))
        #self.autoconnect.set(True)
        Checkbutton(optionbar, text="Afficher automatiquement l'écran distant", variable = self.autoconnect).pack(side =TOP, padx =4)
        self.input = BooleanVar()
        self.input.set(not self.preference['Server'].getboolean('viewonly'))
        Checkbutton(optionbar, text="Partager le clavier et la souris", variable = self.input, command = self.setViewonly).pack(side =TOP, padx =4)
        Button(optionbar,  text="Enregistrer", command= self.save).pack(side =RIGHT, padx =4)

        self.userbar = Frame(sidebar, relief= GROOVE, borderwidth=3)
        self.userbar.pack(side =TOP, padx =4, anchor=NW, fill=X)
        Label(self.userbar, text = "Voir l'écran de:").pack(side =TOP, padx =4, pady =4)
        self.msg_user = Message(self.userbar,
                text = "Personne ne partage son écran pour l'instant.\n\n" +
                "Si vous utilisez un pare-feu, pensez à ouvrir les ports " +
                self.preference['Port']['vnclic'] + " et " + self.preference['Port']['vnc'] + ".",
                width=300, relief=FLAT)
        self.messageUser()

    def showError(self, message=""):
        Message(self.w,
            text = message,
            width=300, relief=FLAT).pack(side =TOP, padx =4)
        Button(self.w,  text="Quitter", command= self.quit).pack(side =TOP, padx =4)

    def connect(self):
        self.server = UDPServer(self.preference['Port'].getint('vnclic'), self)
        print("serveur lancé")
        self.client = UDPClient(self.preference['Port'].getint('vnclic'))
        self.server.start()
        self.client.start()
        print("### connecté")

    def start(self):
        "Start sharing screen"
        #self.server = subprocess.run(["x11vnc", "-gui", "tray"])
        if self.input.get():
            self.vnc_server = subprocess.Popen(["x11vnc", "-shared", "-noviewonly"])
        else:
            self.vnc_server = subprocess.Popen(["x11vnc", "-shared", "-viewonly"])
        #popen.wait, popen.poll...
        try:
            print("retour:", self.server.returncode)
        except:
            print("code retour indisponible")

        self.btn_start.pack_forget()
        self.btn_stop.pack(side =TOP)
        self.started=True
        # spread on network every 5 seconds
        time.sleep(5) # let some time to X11VNC to start
        self.cron_start = Cron(self.spreadStart,5)
        self.cron_start.start()

    def stop(self):
        "Stop sharing screen"
        #subprocess.run(["x11vnc", "-R", "stop"])
        subprocess.Popen(["x11vnc", "-R", "stop"])
        #self.vnc_server.terminate()  #popen

        try:
            print("retour:", self.server.returncode)
        except:
            print("code retour indisponible")
        self.client.send("#STOP#####" + self.name.get())
        self.btn_start.pack(side =TOP)
        self.btn_stop.pack_forget()
        self.started=False
        try:
            self.cron_start.stop()
        except:
            ""

    def receive(self, data, host):
        if data[0:10]=="#START####":
            self.addUser(host, data[10:])
        elif data[0:10]=="#STOP#####":
            self.removeUser(host)

    def message(self, code):
        if code == 0:
            self.showError("La connexion a échoué. Veuillez vérifier que le programme n'est pas déjà lancé.")
        elif code == 1:
            self.mainWindow()

    def addUser(self, host, name=""):
        if host not in self.user_list:
            name = name[0:15]
            self.user_list[host] =\
                Button(self.userbar, text = host + " " + name,
                command = lambda h=host:self.view(h))
            self.user_list[host].pack(side =TOP, padx =4, pady =4)
            self.messageUser()
            if self.autoconnect.get():
                self.view(host)

    def removeUser(self, host):
        if host in self.user_list:
            self.user_list[host].destroy()
            del(self.user_list[host])
        self.messageUser()

    def messageUser(self):
        if len(self.user_list) == 0:
            self.msg_user.pack(side =TOP, padx =4)
        else:
            self.msg_user.pack_forget()

    def setViewonly(self):
        if self.input.get():
            subprocess.Popen(["x11vnc", "-R", "noviewonly"])
        else:
            subprocess.Popen(["x11vnc", "-R", "viewonly"])

    def view(self, host):
        "Execute the remote desktop viewer"
        try:
            self.viewer.terminate()
        except:
            "nothing"
        #self.viewer = subprocess.run(["ssvncviewer", "-scale", "0.75", host])
        self.viewer = subprocess.Popen(["ssvncviewer", "-scale", "0.75", host])

    def spreadStart(self):
        "Say that we are sharing our screen"
        if self.started:
            self.client.send("#START####" + self.name.get())
            return True
        return False

    def save(self):
        "Save preferences"

    def save(self):
        config = configparser.ConfigParser()
        config['Server'] = {}
        config['Server']['viewonly'] = str(not self.input.get())
        config['Server']['name'] = self.name.get()

        config['Client']={}
        config['Client']['scale'] = self.preference['Client']['scale']
        config['Client']['autoconnect'] = str(self.autoconnect.get())

        config['Port']={}
        config['Port']['vnc'] = self.preference['Port']['vnc']
        config['Port']['vnclic'] = self.preference['Port']['vnclic']

        self.config.save(config)

    def quit(self):
      try:
        self.stop()
      except:
        pass
      self.w.quit()

if __name__ == '__main__' :
    VnClic()
