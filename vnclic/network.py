#!/usr/bin/env python3

    #  Copyright Ludovic Kiefer 2020
    
    #  This file is part of vnclic.
      
    #  vnclic is free software: you can redistribute it and/or modify
    #  it under the terms of the GNU General Public License as published by
    #  the Free Software Foundation, either version 2 of the License, or
    #  (at your option) any later version.
      
    #  vnclic is distributed in the hope that it will be useful,
    #  but WITHOUT ANY WARRANTY; without even the implied warranty of
    #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #  GNU General Public License for more details.
      
    #  You should have received a copy of the GNU General Public License
    #  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

from socket import *
import threading, time, sys

def getIP():
    s = socket(AF_INET, SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip

class UDPServer(threading.Thread):
    "Receive UDP messages from the network"

    def __init__(self, port, interface = False):
        threading.Thread.__init__(self)
        self.setDaemon(True)

        self.ip = getIP()
        print("My IP address: " + self.ip);

        self.s=socket(AF_INET, SOCK_DGRAM)
        try:
            self.s.bind(('',port))
            self.status=1
            self.interface = interface
        except:
            self.status=0
        interface.message(self.status)

    def run(self):
        while self.status == 1:
            m=self.s.recvfrom(4096)
            data=m[0].decode("Utf8")
            host=m[1][0]
            if self.interface:
                if host == self.ip:
                    print("Dropped")
                else:
                    self.interface.receive(data,host)
            else:
                print("\nReceived: ",data)
                print("\nFrom: " ,host )


    def stop(self):
        self.status=0

class UDPClient(threading.Thread):
    "Send UDP messages on the network"

    def __init__(self, port):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.s=socket(AF_INET, SOCK_DGRAM)
        self.s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        self.port=port

    def send(self, message, address="255.255.255.255"):
        "Send broadcast (default) or unicast (if address specified) messages"
        try:
            self.s.sendto(message.encode("Utf8"),(address,self.port))
        except:
            print ('UDPClient : Not connected.')

class Cron(threading.Thread):
    "Execute an action every few seconds"

    def __init__(self, action, delay):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.delay = delay
        self.action = action
        self.status = True

    def run(self):
        while self.status:
            #Stops when action() returns False
            self.status = self.action()
            time.sleep(self.delay)

    def stop(self):
        self.status = False

if __name__ == '__main__' :
    port=12345
    listen=UDPServer(port)
    talk=UDPClient(port)
    listen.start()
    print("thread 'listen' started")
    talk.start()
    print("thread 'talk' started")          

    time.sleep(1)
    # Braodcast message
    talk.send("test")
    # Unicast message
    talk.send("hello", "127.0.0.1")

    time.sleep(1)
    print("\nPress return to continue")
    input()
